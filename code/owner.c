#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "library.h"

void owner_area(user_t *u){
    int choice;
    do
    {
        printf("\n\n0. Sign out\n1. Appoint librarian\n2. Edit profile\n3. Change password\n4. Fees Report\n5. Fine Report\n6. Book availablity\n7. Book categories\n Enter choice :");
        scanf("%d",&choice);
        switch (choice)
        {
        case 1://appoint librarian
            appoint_librarian();
            break;
        case 2:// edit profile
            edit_profile(u);
            break;
        case 3://change password
            change_password(u);
            break;
        case 4://Fees and fine Report
            fees_report();
            break;
        case 5://fine report
            fine_report();
            break;
        case 6://book availablity report
            bookcopy_checkavail_details();
            break;
        case 7://book category/subject
            book_category();
            break;
        }
    } while (choice!=0);  
}

void appoint_librarian() {
    //input librarian details
    user_t u;
    u.id=get_next_user_id();
    user_accept(&u);
    //change user role to librarian
    strcpy(u.role, ROLE_LIBRARIAN);
    //add librarian to users file
    user_add(&u);
}

void fees_report() {
    FILE *fp;
    payment_t p;
    int temp=1;
    double fees_amount =0, fine_amount=0;
    date_t t;    
    fp = fopen(PAYMENT_DB, "rb");
    if(fp == NULL){
        perror("can't open payments file");
        exit(1);
    }
    while(fread(&p, sizeof(payment_t), 1, fp)> 0)
    {
        if(temp>0)
        {
            temp--;
            printf("from date : %d /%d /%d ",p.tx_time.day,p.tx_time.month,p.tx_time.year);
        }
        if(strcmp("fees",p.type)==0)
        {
            fees_amount=fees_amount+p.amount;
        }
        if(strcmp("fine",p.type)==0)
        {
            fine_amount=fine_amount+p.amount;
        }
    }
    t=date_current();
    printf("\n to ");
    date_print(&t);
    printf("\nfees amount : %lf",fees_amount);
    printf("\nfine amount : %lf",fine_amount);
    fclose(fp);
}

void book_category() {

    book_t b;
    bookcopy_t c;
    FILE *f, *x;
    f=fopen(BOOK_DB, "rb");
    if(f==NULL) {
        perror("cant open books file");
        exit(1);
    }
    x=fopen(BOOKCOPY_DB, "rb");
    if(x==NULL) {
        perror("cant open book copies file");
        exit(1);
    }
    while(fread(&b, sizeof(book_t),1, f) > 0)
    {
        book_display(&b);
        while(fread(&c, sizeof(bookcopy_t), 1, x) > 0)
        {
            if(b.id == c.bookid)
            bookcopy_display(&c);
        }
    
    }
    fclose(f);
    fclose(x);
}

void fine_report()
{
    FILE *fp;
    payment_t p;
    fp=fopen(PAYMENT_DB,"rb");
    double fine=0;
    date_t d1,d2;
    printf("enter the intial date: \n");
    date_accept(&d1);
    printf("enter the final date: ");
    date_accept(&d2);
    if(fp == NULL) {
		perror("cannot open payment record file");
		exit(1);
	
    }
    while(fread(&p,sizeof(payment_t),1,fp)>0)
    {
        if(date_cmp(p.tx_time,d2)<=0)
        if(strcmp(p.type,PAY_TYPE_FINE)==0)
        fine+=p.amount;
    }
    fclose(fp);
    printf("from date : ");
    date_print(&d1);
    printf("to date : ");
    date_print(&d2);
    printf("Fine : %0.2lf\n",fine);
}
