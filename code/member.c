#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "library.h"

void member_area(user_t *u){
    int choice;
    char name[80];
    do
    {
        printf("\n\n0. Sign out\n1. Find book\n2. Edit profile\n3. Change password\n4. Book availablity\n5 display issued books \n6 payment history\nEnter choice :");
        scanf("%d",&choice);
        switch (choice)
        {
        case 1://find book
            printf("enter book name");
            scanf("%s",name);
            book_find_by_name(name);
            break;
        case 2:// edit profile
            edit_profile(u);
            break;
        case 3:// change password
            change_password(u);
            break;
        case 4://check availablity
            bookcopy_checkavail();
            break;
        case 5://display issued books
            display_issued_books(u->id);
            break;
        case 6://payment history
            payment_history(u->id);
            break;
        }
    } while (choice!=0);  
}

void bookcopy_checkavail() {
    int book_id; 
    FILE *fp;
	bookcopy_t bc;
	int count = 0;
	// input book id
	printf("enter the book id: ");
	scanf("%d", &book_id);
	// open book copies file
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL) {
		perror("cannot open bookcopies file.");
		return;
	}
	// read bookcopy records one by one
	while(fread(&bc, sizeof(bookcopy_t), 1, fp) > 0) {
		// if book id is matching and status is available, print copy details
		if(bc.bookid == book_id && strcmp(bc.status, STATUS_AVAIL) == 0) {
			//bookcopy_display(&bc);
			count++;
		}
	}
	// close book copies file
	fclose(fp);
	//print the message. 
		printf("number of copies available = %d \n",count);
}

void display_issued_books(int id)
{
    FILE *fp;
    issuerecord_t i;
    fp=fopen(ISSUERECORD_DB,"rb");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    while(fread(&i,sizeof(issuerecord_t),1,fp)>0){
        if(i.memberid==id)
        issuerecord_display(&i);
    }
   
    fclose(fp);
}
