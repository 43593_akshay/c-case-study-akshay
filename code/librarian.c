#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "library.h"


void librarian_area(user_t *u){
    int choice, id;
    char name[80];
    do
    {
        
        printf("\n\n0. Sign out\n1 Add member\n2 Edit profile\n3 Change password\n4 Add book\n5 Find book\n6 Edit book\n7check availablity\n8 Add copy\n9 Change rack\n10 Issue copy\n11 Return copy\n12 Take payment\n13 Payment History\n14 Issued books list\n15 display all members\nEnter Choice:");
        scanf("%d",&choice);
        switch (choice){
        case 1://add member
            add_member();
            break;
        case 2:// edit profile
            edit_profile(u);
            break;
        case 3:// change password
            change_password(u);
            break;
        case 4://add book
            add_book();
            break;
        case 5://find book
            printf("enter book name");
            scanf("%s",name);
            book_find_by_name(name);
            break;
        case 6: // edit book by id
            book_edit_by_id();
            break;
        case 7://check availablity
            bookcopy_checkavail_details();
            break;
        case 8://add copy
            bookcopy_add();
            break;
        case 9://change rack
		    change_rack();
            break;
        case 10: // Issue Book Copy
			bookcopy_issue();
            break;
        case 11: // Return Book Copy
		    bookcopy_return();
            break;
        case 12:// take payment
            take_payment();
            break;
        case 13:// payment history
            printf("User id");
            scanf("%d", &id);
            payment_history(id);
            break;
	    case 14://issued books list
		    list_issued_books();	
            break;
        case 15: // list all members
            list_all_members();
            break;
        }
    } while (choice!=0);
}

void add_member() {
    //input member details
    user_t u;
    user_accept(&u);
    //add member to users file
    user_add(&u);
}

void add_book() {
    FILE *fp;
    //input book details
    book_t b;
    book_accept(&b);
    b.id=get_next_book_id();
    //add book into books file
    //open books file
    fp =fopen(BOOK_DB, "ab");
    if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
    }
    //append book to the file
    fwrite(&b, sizeof(book_t), 1, fp);
    printf("book added int the file \n");
    //close the file
    fclose(fp);
}

void book_edit_by_id(){
    FILE *fp;
    int id, found = 0;
    book_t b;
    //input book id from user
    printf("enter book id :");
    scanf("%d", &id);
    //open books file
    fp = fopen(BOOK_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}
    //read book one by one and check if book with given id is found 
    while(fread(&b, sizeof(book_t), 1, fp) > 0) {
		if(id == b.id) {
			found = 1;
			break;
		}
	}
    //if found
    if(found) {
        //input new book details from user
        long size = sizeof(book_t);
        book_t nb;
        book_accept(&nb);
        nb.id = b.id;
        //take file position one record behind
        fseek(fp, -size, SEEK_CUR);
        //overwrite book details into the file
        fwrite(&nb, size, 1, fp);
        printf("book updated. \n");
    }
    else //if not found
             //show massege to user that book not found
             printf("book not found \n");
    //close books file
    fclose(fp);
}

void bookcopy_add() {
    FILE *fp;
    //input bookcopy details
    bookcopy_t b;
    bookcopy_accept(&b);
    b.id=get_next_bookcopy_id();
    //add bookcopy into book copies file
    //open book copies file
    fp =fopen(BOOKCOPY_DB, "ab");
    if(fp == NULL) {
		perror("cannot open book copies file");
		exit(1);
    }
    //append book copies to the file
    fwrite(&b, sizeof(bookcopy_t), 1, fp);
    printf("book copy added into the file \n");
    //close the book copies file
    fclose(fp);
}

void bookcopy_checkavail_details() {
    int book_id; 
    FILE *fp;
	bookcopy_t bc;
	int count = 0;
	// input book id
	printf("enter the book id: ");
	scanf("%d", &book_id);
	// open book copies file
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL) {
		perror("cannot open bookcopies file.");
		return;
	}
	// read bookcopy records one by one
	while(fread(&bc, sizeof(bookcopy_t), 1, fp) > 0) {
		// if book id is matching and status is available, print copy details
		if(bc.bookid == book_id && strcmp(bc.status, STATUS_AVAIL) == 0) {
			bookcopy_display(&bc);
			count++;
		}
	}
	// close book copies file
	fclose(fp);
	// if no copy is available, print the message. 
	if(count == 0)
		printf("no copies availables \n");
}

void bookcopy_issue() {
	issuerecord_t rec;
	FILE *fp;
	// accept issuerecord details from user
	issuerecord_accept(&rec);
	//if user is not paid, give error & return.
    if(!is_paid_user(rec.memberid)) {
		printf("Member is not paid \n");
		return;
	}
	// generate & assign new id for the issuerecord
	rec.id = get_next_issuerecord_id();
	// open issuerecord file
	fp = fopen(ISSUERECORD_DB, "ab");
	if(fp == NULL) {
		perror("issuerecord file cannot be opened");
		exit(1);
	}
	// append record into the file
	fwrite(&rec, sizeof(issuerecord_t), 1, fp);
	// close the file
	fclose(fp);

	// mark the copy as issued
	bookcopy_changestatus(rec.copyid, STATUS_ISSUED);
}

void bookcopy_changestatus(int bookcopy_id, char status[]) {
	bookcopy_t bc;
	FILE *fp;
	long size = sizeof(bookcopy_t);
	// open book copies file
	fp = fopen(BOOKCOPY_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open book copies file");
		return;
	}
	// read book copies one by one
	while(fread(&bc, sizeof(bookcopy_t), 1, fp) > 0) {
		// if bookcopy id is matching
		if(bookcopy_id == bc.id) {
			// modify its status
			strcpy(bc.status, status);
			// go one record back
			fseek(fp, -size, SEEK_CUR);
			// overwrite the record into the file
			fwrite(&bc, sizeof(bookcopy_t), 1, fp);
			break;
		}
	}
	// close the file
	fclose(fp);
}

void display_issued_bookcopies(int member_id) {
	FILE *fp;
	issuerecord_t rec;
	// open issue records file
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp==NULL) {
		perror("cannot open issue record file");
		return;
	}
	// read records one by one
	while(fread(&rec, sizeof(issuerecord_t), 1, fp) > 0) {
		// if member_id is matching and return date is 0, print it.
		if(rec.memberid == member_id && rec.return_date.day == 0)
			issuerecord_display(&rec);
	}
	// close the file
	fclose(fp);
}

void bookcopy_return() {
	int member_id, record_id;
	FILE *fp;
	issuerecord_t rec;
	int diff_days, found = 0;
	long size = sizeof(issuerecord_t);
	// input member id
	printf("enter member id: ");
	scanf("%d", &member_id);
	// print all issued books (not returned yet)
	display_issued_bookcopies(member_id);
	// input issue record id to be returned.
	printf("enter issue record id (to return): ");
	scanf("%d", &record_id);
	// open issuerecord file
	fp = fopen(ISSUERECORD_DB, "rb+");
	if(fp==NULL) {
		perror("cannot open issue record file");
		return;
	}
	// read records one by one
	while(fread(&rec, sizeof(issuerecord_t), 1, fp) > 0) {
		// find issuerecord id
		if(record_id == rec.id) {
			found = 1;
			// initialize return date
			rec.return_date = date_current();
			// check for the fine amount
			diff_days = date_cmp(rec.return_date, rec.return_duedate);
			// update fine amount if any
			if(diff_days > 0)
			{	
                rec.fine_amount = diff_days * FINE_PER_DAY;
				fine_payment(rec.memberid, rec.fine_amount);
                printf("Fine of Rs. %.2lf paid\n",rec.fine_amount);
            }
			break;
		}
	}
	if(found) {
		// go one record back
		fseek(fp, -size, SEEK_CUR);
		// overwrite the issue record
		fwrite(&rec, sizeof(issuerecord_t), 1, fp);
		// print updated issue record.
		printf("issue record updated after returning book:\n");
		issuerecord_display(&rec);
		// update copy status to available 
		bookcopy_changestatus(rec.copyid, STATUS_AVAIL);
	}
	
	// close the file.
	fclose(fp);
}

void change_rack()
{
    int copy_id,found=0;
    FILE *fp;
    bookcopy_t rec;
    printf("Enter the Copy id whose rack has to be changed: ");
    scanf("%d",&copy_id);

    fp=fopen(BOOKCOPY_DB,"rb+");
    while(fread(&rec,sizeof(bookcopy_t),1,fp)>0)
    {
        if(rec.id==copy_id)
        {
            found=1;
            break;
        }
    }
    if(found)
    {
        int rack;
        printf("Enter the rack number to which the copy is to be transferred: ");
        scanf("%d",&rack);
        rec.rack=rack;
        fseek(fp,-sizeof(bookcopy_t),SEEK_CUR);
        fwrite(&rec, sizeof(bookcopy_t),1, fp);
        printf("Rack changed successfully\n");
        bookcopy_display(&rec);
    }
    else 
    printf("Bookcopy not found!!\n");
    fclose(fp);
}

void list_issued_books()
{
    FILE *fp;
    int flag=0;
    issuerecord_t rec;
    fp=fopen(ISSUERECORD_DB,"rb");
    if(fp==NULL) {
		perror("cannot open issue record file");
		return;
	}
    while(fread(&rec, sizeof(issuerecord_t),1,fp)>0)
    {
        issuerecord_display(&rec);
        flag=1;
    }
    if(flag==0)
    printf("No books issued.\n");
    fclose(fp);
}

void list_all_members()
{
    user_t u;
    FILE *fp;
    fp=fopen(USER_DB,"rb");
     if(fp==NULL) {
		perror("cannot open issue record file");
		return;
	}
    while(fread(&u,sizeof(user_t),1,fp)>0)
    user_display(&u);
    fclose(fp);
}

void take_payment()
{
    payment_t p;
    FILE *fp;
    strcpy(p.type,PAY_TYPE_FEES);
    p.id=get_next_payment_id();
    payment_accept(&p);
    fp=fopen(PAYMENT_DB,"ab");
    if(fp==NULL) {
		perror("cannot open payments record file");
		return;
	}
  fwrite(&p,sizeof(payment_t),1,fp);
  fclose(fp);
}

int is_paid_user(int memberid) {
	date_t now = date_current();
	FILE *fp;
	payment_t pay;
	int paid = 0;
	// open file
	fp = fopen(PAYMENT_DB, "rb");
	if(fp==NULL) {
		perror("cannot open payment file");
		return 0;
	}
	// read payment one by one till eof
	while(fread(&pay, sizeof(payment_t), 1, fp) > 0) {
		if(pay.memberid == memberid && pay.next_pay_duedate.day != 0 && date_cmp(now, pay.next_pay_duedate) < 0) {
				paid = 1;
			    break;
		}
	}
	// close file	
	fclose(fp);
	return paid;
}

void fine_payment(int memberid, double fine_amount) {
	FILE *fp;
    payment_t pay;
    printf("ENTERE\n");
	pay.id = get_next_payment_id();
	pay.memberid = memberid;
	pay.amount = fine_amount;
	strcpy(pay.type, PAY_TYPE_FINE);
	pay.tx_time = date_current();
	memset(&pay.next_pay_duedate, 0, sizeof(date_t));
	fp = fopen(PAYMENT_DB, "ab");
	if(fp == NULL) {
		perror("cannot open payment file");
		exit(1);
	}
	fwrite(&pay, sizeof(payment_t), 1, fp);
	fclose(fp);
}